package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserOwnedRepository;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository <M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M>
{
    @Override
    public void clear(String userId) {
        final List<M> models = findAll(userId);
        for (final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m: models) {
            if (userId.equals(m.getUserId())) result.add(m);
        }
        return result;

    }

    @Override
    public List<M> findAll(String userId, Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(String userId, String id) {
        if (userId == null || id == null) return null;
        for (final M m : models) {
            if (id.equals(m.getId())) continue;
            if (!userId.equals(m.getUserId())) continue;
            return m;
        }
        return null;

    }

    @Override
    public M findOneByIndex(String userId, Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(String userId) {
        int count = 0;
        for (final M m : models) {
            if (userId.equals(m.getUserId())) count++;
        }
        return count;
    }

    @Override
    public M removeById(String userId, String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(String userId, Integer index) {
        if (userId == null || index == null) return null;
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M add(String userId, M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);

    }

    @Override
    public M remove(String userId, M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }
}
