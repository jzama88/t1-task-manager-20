package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.exception.field.UserIdEmptyException;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository <M extends AbstractModel>
{
    M add (M model) throws AbstractEntityNotFoundException;

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(String userId,Sort sort) throws UserIdEmptyException, AbstractFieldException;

    M findOneById(String id) throws IdEmptyException, AbstractFieldException;

    M findOneByIndex(Integer index) throws IndexIncorrectException, AbstractFieldException;

    M remove(M model) throws AbstractEntityNotFoundException;

    M removeById(String id) throws AbstractFieldException, AbstractEntityNotFoundException;

    M removeByIndex(String userId, Integer index) throws IndexIncorrectException, AbstractFieldException;

    void clear();

    int getSize();

    boolean existsById(String id);


}
