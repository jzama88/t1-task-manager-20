package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.service.ILoggerService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    public static final String CONFIG_FILE = "/logger.properties";

    public static final String COMMANDS = "COMMANDS";

    public static final String COMMANDS_FILE = "./commands.xml";

    public static final String ERRORS = "ERRORS";

    public static final String ERRORS_FILE = "./errors.xml";

    public static final String MESSAGES = "MESSAGES";

    public static final String MESSAGES_FILE = "./messages.xml";

    public static final LogManager MANAGER = LogManager.getLogManager();

    public static final Logger LOGGER_ROOT = Logger.getLogger("");

    public static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    public static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    public static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    private final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    public static Logger getLoggerCommand() {
        return Logger.getLogger(COMMANDS);
    }

    public static Logger getLoggerError() {
        return LOGGER_ERROR;
    }

    public static Logger getLoggerMessage() {
        return LOGGER_MESSAGE;
    }

    {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);

    }

    private void loadConfigFromFile() {
        try {
            final Class<?> clazz = LoggerService.class;
            final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private static ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(
            final Logger logger,
            final String fileName,
            final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if ((message == null) || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void command(final String message) {
        if ((message == null) || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void debug(final String message) {
        if ((message == null) || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void error(Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }
}
