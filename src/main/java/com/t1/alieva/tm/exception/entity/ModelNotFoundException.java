package com.t1.alieva.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException{

    public ModelNotFoundException() { super("Error! Model not found..."); }
}
