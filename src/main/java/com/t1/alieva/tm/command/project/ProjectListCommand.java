package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand{

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public void execute() throws AbstractUserException, AbstractFieldException {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId,sort);
        int index = 1;
        for(final Project project:projects){
            System.out.println(index + ". " + project);
            index++;
        }
    }
}
